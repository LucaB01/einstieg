package at_Luca_Encrypter;

import java.util.ArrayList;

public class Cesar extends AbstractEncrypter{
	private String founder = "Joe";

	public Cesar(String founder) {
		super(founder);
	}

	@Override
	public void encrypt(String txt, int n) {
		
		char[] alphabet = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
		char[] text = txt.toCharArray();
		ArrayList<String> encrypted = new ArrayList<String>();
 		
		for (int i=0; i<txt.length(); i++) {
			for(int z = 0; z<alphabet.length; z++){
			    if(text[i] == alphabet[z]){
			    	String s=String.valueOf(alphabet[z+n]);		
			        encrypted.add(s);
			        break;
			    }
			}
		}
		System.out.println(encrypted);
	}

	@Override
	public String getFounder() {
		return("Wurde programmiert von " + founder);
	}
}
