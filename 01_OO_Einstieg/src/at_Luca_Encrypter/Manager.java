package at_Luca_Encrypter;

import java.util.ArrayList;
import java.util.List;

public class Manager {
	private List<Encrypter> encrypter;
	
	public Manager() {
		encrypter = new ArrayList<>();
	}
	
	public void encryptAll(String txt, int n) {
		for (Encrypter e : encrypter) {
			e.encrypt(txt,n);
		}
	}
	
	public void addEncrypter(Encrypter e) {
		this.encrypter.add(e);
	}

}
