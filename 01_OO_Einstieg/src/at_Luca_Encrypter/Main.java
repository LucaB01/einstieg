package at_Luca_Encrypter;

public class Main {
	public static void main(String[] args) {
		Manager m1 = new Manager();
		Encrypter c1 = new Cesar("s");
		Encrypter a1 = new Algo1("Albert");
		Encrypter a2 = new Algo2("Hans");
		
		m1.addEncrypter(c1);
		m1.addEncrypter(a1);
		m1.addEncrypter(a2);
		c1.encrypt("TestZz",3);
	}
}
