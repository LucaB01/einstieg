package at_Luca_Encrypter;

public class Algo1 extends AbstractEncrypter{

	public Algo1(String founder) {
		super(founder);
	}

	@Override
	public void encrypt(String txt, int n) {
		StringBuffer result= new StringBuffer(); 
		  
        for (int i=0; i<txt.length(); i++) 
        { 
            if (Character.isUpperCase(txt.charAt(i))) 
            { 
                char ch = (char)(((int)txt.charAt(i) + 
                                  3 - 65) % 26 + 65); 
                result.append(ch); 
            } 
            else
            { 
                char ch = (char)(((int)txt.charAt(i) + 
                                  3 - 97) % 26 + 97); 
                result.append(ch); 
            } 
        } 
		System.out.println(result);
	}

	@Override
	public String getFounder() {
		return ("Wurde programmiert von " + founder);
	}

}
