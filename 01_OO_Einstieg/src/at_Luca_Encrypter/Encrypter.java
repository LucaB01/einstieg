package at_Luca_Encrypter;

public interface Encrypter {
	public void encrypt(String txt, int n);
	public String getFounder();
}
