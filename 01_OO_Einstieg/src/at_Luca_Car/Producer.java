package at_Luca_Car;

public class Producer {
		private String name;
		private String homeland;
		private double discount;
		
		public Producer(String name, String homeland, double discount) {
			super();
			this.name = name;
			this.homeland = homeland;
			this.discount = discount;
		}

		public String getName() {
			return name;
		}

		public String getHomeland() {
			return homeland;
		}

		public double getDiscount() {
			double discountValue;
			discountValue = discount * 0.01;
			return discountValue;
		}

		public void setDiscount(double discount) {
			this.discount = discount;
		}
		
			
		
}
