package at_Luca_Car;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstName;
	private String lastName;
	private LocalDate birthday;
	private List<Car> cars;
	
	public Person(String firstName, String lastName, LocalDate birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.cars = new ArrayList<>();
	}
	
	public void printCars () {
		for (Car car : cars) {
			System.out.println(car.getColor());
		}
	}
	
	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
	public void printValueOfCars() {
		double price = 0;
		for (Car car : cars) {
			double preis = car.price();
			price = price+preis;
		}
		System.out.println(price);
	}
	
	public String getBirthday () {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("[yyyy-MM-dd][dd/MM/yyyy][MM-dd-yyyy]");
		String formattedString = birthday.format(formatter);
	    return formattedString;
	}
	
	public int getAge () {
		 LocalDate currentDate = LocalDate.now();
	     return Period.between(birthday, currentDate).getYears();
	}
}
