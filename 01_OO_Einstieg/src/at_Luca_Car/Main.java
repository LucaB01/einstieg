package at_Luca_Car;

import java.time.LocalDate;
import java.util.Date;

public class Main {
	public static void main(String[] args) {
		Person per1 = new Person("Julian","Grie�er", LocalDate.of(2001,02,24));
		
		Producer p1 = new Producer("Kia","Japan",5);
		
		Engine e1 = new Engine(150, "Diesel");
		
		Car c1 = new Car("Black",230,15000,10,75000,e1,p1);
		Car c2 = new Car("red",280,35000,12,23000,e1,p1);
		
		

		
		per1.addCar(c1);
		per1.addCar(c2);
		
		per1.printCars();
		System.out.println(c1.price());
		System.out.println(c2.price());
		per1.printValueOfCars();
		System.out.println(per1.getAge());
		
		
		System.out.println(e1.getType());
		//System.out.println(c1.getBasicPrice());
		//System.out.println(p1.getDiscount());
		
		System.out.println(c1.getBasicConsumption());
	}
	 
}
