package at_Luca_Car;

public class Engine {
		private int hp;
		private String type;
		
		public Engine(int hp, String type) {
			super();
			this.hp = hp;
			this.type = type;
		}

		public int getHp() {
			return hp;
		}

		public String getType() {
			if (type == "Diesel" || type == "Benzin") {
			return type;
			}
			else {
				return "Diese Art ist nicht g�ltig!";
			}
		}
}
