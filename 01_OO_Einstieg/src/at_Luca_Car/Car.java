package at_Luca_Car;
public class Car {
	public String color;
	private int maxSpeed;
	private double basicPrice;
	private double basicConsumption;
	private double distance;
	private Engine engine;
	private Producer producer;
	
	public Car(String color, int maxSpeed, double basicPrice, double basicConsumption, double distance, Engine engine, Producer producer) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basicPrice = basicPrice;
		this.basicConsumption = basicConsumption;
		this.distance = distance;
		this.engine = engine;
		this.producer = producer;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public double getBasicPrice() {
		return basicPrice;
	}

	public void setBasicPrice(double basicPrice) {
		this.basicPrice = basicPrice;
	}

	public double getBasicConsumption() {
	if (distance <= 50000) {
		return basicConsumption;
	}
	else {
		basicConsumption = basicConsumption*1.098;
		return basicConsumption;
		}
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public double price() {
		double price;
		price = basicPrice * (1.00-producer.getDiscount());
		return price;
	}
}
