package at_Luca_FactoryPattern;

public class RandomActorFactory {
	private int x;
	
	public Actors randomActor() {
		x = (int)(Math.random() * ((3 - 1) + 1)) + 1;
		
		if (x == 1) {
			Actors h = new Homer();
			return h;
		}
		else if (x == 2) {
			Actors sf = new Snowflake();
			return sf;
		}
		else if (x == 3) {
			Actors sb = new Snowball();
			return sb;
		}
		else {
			return null;
		}
	}
	
}
