package at_Luca_FactoryPattern;

public interface Actors {
	public void name();
	public void render();
}
