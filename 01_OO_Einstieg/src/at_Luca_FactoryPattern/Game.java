package at_Luca_FactoryPattern;

import java.util.ArrayList;
import java.util.List;

import at_Luca_ObserverPattern.Observable;

public class Game {
	private List<Actors> actors;
	
	public Game() {
		this.actors = new ArrayList<>();
	}
	
	public void addRandomActor() {
		RandomActorFactory ra = new RandomActorFactory();
		actors.add(ra.randomActor());
	}
	
	public void nameAll() {
		for (Actors a : actors) {
			a.name();
		}
	}
}
