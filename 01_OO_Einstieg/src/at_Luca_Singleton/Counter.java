package at_Luca_Singleton;

public class Counter {
	private int counter;
	private static Counter c1;
	
	private Counter(){
		
	}
	
	public static Counter getInstance() {
		if (c1 == null) {
			c1 = new Counter();
		}
		return c1;
	}
	
	public void add() {
		counter++;
		System.out.println(counter);
	}

}
