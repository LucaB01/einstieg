package at_Luca_Singleton;

public class Main {
	public static void main(String[] args) {
		Counter c1 = Counter.getInstance();
		Counter c2 = Counter.getInstance();
		Counter c3 = Counter.getInstance();
		
		c3.add();
		c2.add();
		c1.add();
	}
}
