package at_Luca_ConnectFour;

public class Array {
	String[][] dimensionalArray;
	int row;
	int column;
	
	public Array(int row, int column) {
		super();
		this.row = row;
		this.column = column;
		dimensionalArray = new String[row][column];
	}
	
	
	public String[][] returnArray() {
        return dimensionalArray;
    }
	
	public String getArray(int r, int c) {
		return dimensionalArray[r][c];
	}
	
	public void addToArray(int r, int c, String text) {
		dimensionalArray[r][c] = text;
	}
	
	public int getColumnLength() {
		return column;
	}
	
	public String[] getOneColumn(int c) {
		return dimensionalArray[c];
	}
	
	public int getRowLength() {
		return row;
	}
	
	public String[] getOneRow(int r) {
		return dimensionalArray[r];
	}
}
