package at_Luca_ConnectFour;

public class Player {
	private String playername;

	public Player(String playername) {
		super();
		this.playername = playername;
	}
	
	public String getPlayername() {
		return this.playername;
	}

}
