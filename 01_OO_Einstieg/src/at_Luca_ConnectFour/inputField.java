package at_Luca_ConnectFour;

import javax.swing.JOptionPane;

public class inputField {
	private String input;
	
	public void getInputField() {
		String input = JOptionPane.showInputDialog(null,
			 "Geben Sie Ihren Namen ein",
			 "Spielername",
			 JOptionPane.QUESTION_MESSAGE);
		this.input = input;
	}
	
	public String getInput() {
		return input;
	}
	
}
