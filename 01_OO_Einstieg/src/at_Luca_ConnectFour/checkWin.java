package at_Luca_ConnectFour;

public class checkWin {
		private int counter1 = 0;
		private int counter2 = 0;
		private int counter3 = 0;
		private int counter4 = 0;
		private int counter5 = 0;
		private int counter6 = 0;
		private int saveRow1;
		private int saveRow2;
		private int saveRow3;
		private int saveRow4;
		private int saveColumn1;
		private int saveColumn2;
		private int saveColumn3;
		private int saveColumn4;
		int setValues = 0;
		public boolean gameFinished = false;
		
			
		void checkAll(Array array) {
			checkHorizontal(array);
			if (!gameFinished) {
				checkVertical(array);
			}
			if (!gameFinished) {
				checkDiagonal(array);
			}
			if (!gameFinished) {
				checkDiagonal2(array);
			}
			if (!gameFinished) {
				checkDiagonal3(array);
			}
			if (!gameFinished) {
				checkDiagonal4(array);
			}
		}
		
		void saveValues (int r, int c) {
			switch(setValues) {
			case 0:
				saveRow1 = r;
				saveColumn1 = c;
				break;
			case 1:
				saveRow2 = r;
				saveColumn2 = c;
				break;
			case 2:
				saveRow3 = r;
				saveColumn3 = c;
				break;
			case 3:
				saveRow4 = r;
				saveColumn4 = c;
				break;
			}
			setValues++;
		}
		
		boolean getArray1 (int r, int c) {
		
			boolean match = false;
			if (saveRow1 == r && saveColumn1 == c) {
				match = true;
			}
			if (saveRow2 == r && saveColumn2 == c) {
				match = true;
			}
			if (saveRow3 == r && saveColumn3 == c) {
				match = true;
			}
			if (saveRow4 == r && saveColumn4 == c) {
				match = true;
			}
			return match;

		}
		
		void removeVarValues() {
			if (!gameFinished) {
				saveRow1 = 100;
				saveRow2 = 100;
				saveRow3 = 100;
				saveRow4 = 100;
				saveColumn1 = 100;
				saveColumn2 = 100;
				saveColumn3 = 100;
				saveColumn4 = 100;
			}
		}

		void checkHorizontal(Array array) {
			for (int x = 0; x < array.getColumnLength(); x++) {
				counter1 = 0;
				for (int i = 0; i < array.getRowLength(); i++) {
				if (!gameFinished) {
					if (i != 0) {
						if (array.getArray(i, x) == array.getArray(i-1, x) && array.getArray(i-1, x) != null) {
							counter1++;
							saveValues(i-1,x);
						}
						else {
							setValues = 0;
							counter1 = 0;
						}
						if (counter1 == 3) {
							gameFinished = true;
							saveValues(i,x);
						}
					}
				}
				}
			}
			removeVarValues();
			setValues = 0;
		}
		
		
		
		void checkVertical(Array array) {
			for (int i = 0; i < array.getRowLength(); i++) {
				counter2 = 0;
				for (int x = 0; x < array.getColumnLength(); x++) {
					if (x != 0 && !gameFinished) {
						if (array.getArray(i, x) == array.getArray(i, x-1) && array.getArray(i, x-1) != null) {
							counter2++;
							saveValues(i,x);
						}
						else {
							counter2 = 0;
							setValues = 0;
						}
						if (counter2 == 3) {
							gameFinished = true;
							saveValues(i,x-3);
							break;
						}
					}
				}
			}
			removeVarValues();
			setValues = 0;
		}
		
		void checkDiagonal(Array array) {
			for (int o = 0; o < array.getRowLength(); o++) {	//quer von links oben nach rechst unten / nur rechts oben
				int p = 0;
				int i = o;
				counter3 = 0;
				for (int x = 0; x < array.getRowLength(); x++) {				
					if (i < 6 && !gameFinished) { 
						if (p < 5) {
						if (array.getArray(i+1, p+1) == array.getArray(i, p) && array.getArray(i, p) != null) {
							counter3++;
							saveValues(i,p);
						}
						else {
							counter3 = 0;
							setValues = 0;
						}
						if (counter3 == 3) {
							gameFinished = true;
							saveValues(i+1,p+1);
							break;
						}
						}
					}	
					i++;
					p++;
				} 
			}
			removeVarValues();
			setValues = 0;
		}
		
		void checkDiagonal2(Array array) {
			for (int o = 1; o < array.getRowLength(); o++) {	//quer von links oben nach rechst unten / nur links unten
				int p = o;
				int i = 0;
				counter4 = 0;
				for (int x = 0; x < array.getRowLength(); x++) {				
					if (i < 6 && !gameFinished) { 
						if (p < 5) {
						if (array.getArray(i+1, p+1) == array.getArray(i, p) && array.getArray(i, p) != null) {
							counter4++;
							saveValues(i,p);
						}
						else {
							counter4 = 0;
							setValues = 0;
						}
						if (counter4 == 3) {
							gameFinished = true;
							saveValues(i+1,p+1);
							break;
						}
						}
					}	
					i++;
					p++;
				} 
			}
			removeVarValues();
			setValues = 0;
		}
		
		void checkDiagonal3(Array array) {
			for (int o = 5; o >= 0; o--) {
				int i = o;
				counter5 = 0;
				for (int x = 0; x < array.getRowLength(); x++) {
					if (i > 0 && !gameFinished) {			
						if (array.getArray(i, x) == array.getArray(i-1, x+1) && array.getArray(i-1, x+1) != null) {
							counter5++;
							saveValues(i,x);
						}
						else {
							counter5 = 0;
							setValues = 0;
						}
						if (counter5 == 3) {
							gameFinished = true;
							saveValues(i-1,x+1);
							break;
					}
					i--;	
				}
				}
				
			}
			removeVarValues();
			setValues = 0;
		}
		
		void checkDiagonal4(Array array) {
			for (int o = 0; o < array.getColumnLength(); o++) {
				int i = 6;
				int d = o;
				counter6 = 0;
				for (int x = 0; x < array.getRowLength(); x++) {
					if (i > 0 && !gameFinished) {
						if (d < 5) {
						if (array.getArray(i, d) == array.getArray(i-1, d+1) && array.getArray(i-1, d+1) != null) {
							counter6++;
							saveValues(i,d);
						}
						else {
							counter6 = 0;
							setValues = 0;
						}
						if (counter6 == 3) {
							gameFinished = true;
							saveValues(i-1,d+1);
							break;
					}
					i--;
					d++;
					}
				}
				}
				
			}
			removeVarValues();
			setValues = 0;
		}
}
