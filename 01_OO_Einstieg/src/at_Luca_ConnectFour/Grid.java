package at_Luca_ConnectFour;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.*;
import java.awt.event.*;

public class Grid{
	private JButton button2;
	private JButton button1;
	private checkWin cw;
	private Array array;
	private Player person1;
	private Player person2;
	private boolean isPerson1;
	Font font = new Font("Verdana", Font.BOLD, 50);
	
	
	public Grid(int row, int column, String player1, String player2) {
		super();
		array = new Array(row,column);
		cw = new checkWin();
		person1 = new Player(player1);
		person2 = new Player(player2);
		isPerson1 = true;
	}


	public void create() {	
		JFrame frame = new JFrame("Connect four!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 700);
        GridLayout grid = new GridLayout(7, 8, 10,10);
        frame.setLayout(grid);
       
        
        createFields(frame);
        createButtons(frame);
         frame.setVisible(true);
		isPerson1 = !isPerson1;
       
        if (isPerson1) {
        	gameFinished(frame,person1);
        }
        else {
        	gameFinished(frame,person2);
        }
    }
	
	private void createFields(JFrame frame) {
		
		for (int i = 0; i < 6; i++) {
	        for(int x = 0; x < 7 ; x++) {
	        	JTextPane text = new JTextPane();
	    		text.setText(array.getArray(x,i));
	    		text.setFont(font);
	    		if (cw.getArray1(x, i)) {
	    			text.setForeground(Color.GREEN);
	    		}
	    		else {
		    		if (array.getArray(x,i) == "X") {
		    			text.setForeground(Color.RED);
		    		}
		    		else {
		    			text.setForeground(Color.BLUE);
		    		}
	    		}
	    		StyledDocument doc = text.getStyledDocument();
		    		SimpleAttributeSet center = new SimpleAttributeSet();
		    		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);	//den text zentrieren
		    		doc.setParagraphAttributes(0, doc.getLength(), center, false);
	    		text.setEditable(false); 
	    		frame.add(text);
	        }
        }
	}
	
	private void createButtons(JFrame frame) {
		for(int i = 0; i < 7 ; i++) {
        	if (isPerson1) {
        		button1 = createButton("X",i,person1,frame);		
        	}
        	else {
        		button1 = createButton("O",i,person2,frame);  		
        	}
            frame.add(button1);
        }
	}
	
	private void gameFinished(JFrame frame, Player person) {
		if (cw.gameFinished) {
        	JOptionPane.showMessageDialog(frame, person.getPlayername() + " hat gewonnen!");
        	System.exit(0);
        }
	}
	
	public void description() {
		System.out.println(person1.getPlayername() + " = X\n" + person2.getPlayername() + " = O\n"
				+ "Versuchen sie ihre Kennzeichen horizontal, vertikal oder quer zu setzen");
	}
	
	private JButton createButton(final String name, int i,Player person, JFrame frame)
	  {
	    JButton button2 = new JButton(name);
	    button2.addActionListener(new ActionListener()
	    {
	      @Override
	      public void actionPerformed(ActionEvent e)
	      {        
	        for (int x = 5; x >= 0 ; x--) {
		        if (array.getArray(i, x) == null) {
		        	array.addToArray(i,x,name);
		        	break;
		        }
	        }
	        cw.checkAll(array); 
	        create();
	      }
	    });

	    return button2;
	  }	
}


