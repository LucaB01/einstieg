package at_Luca_Interface;

public class Main {
	public static void main(String[] args) {
		Title t1 = new Title();
		Item i1 = new Item();
		Player p1 = new Player();
		Song s1 = new Song();
		
		p1.addPlayable(t1);
		p1.addPlayable(i1);
		p1.addPlayable(s1);
		
		p1.playAll();
	}
}
