package at_Luca_Interface;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private List<Playable> playables;
	
	public Player() {
		this.playables = new ArrayList<>();
	}
	
	public void playAll() {
		for (Playable playable : playables) {
			playable.play();
		}
	}
	
	public void addPlayable(Playable p) {
		this.playables.add(p);
	}
}
