package at_Luca_ObserverPattern;

public interface Observable {
	public void inform();
}
