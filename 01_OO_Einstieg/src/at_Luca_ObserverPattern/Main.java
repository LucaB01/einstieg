package at_Luca_ObserverPattern;

public class Main {
	public static void main(String[] args) {
		Sensor s1 = new Sensor();
		Observable l1 = new Lantern();
		Observable l2 = new Lantern();
		Observable l3 = new Lantern();
		Observable c1 = new Christmas();
		Observable t1 = new Trafficlight();
		
		s1.addObservable(l1);
		s1.addObservable(l2);
		s1.addObservable(c1);
		s1.addObservable(t1);
		s1.informAll();
	}
}
