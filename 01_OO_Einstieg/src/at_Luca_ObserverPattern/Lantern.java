package at_Luca_ObserverPattern;

public class Lantern implements Observable{

	@Override
	public void inform() {
		
		System.out.println("Lanternen leuchten!");	
	}

}
