package at_Luca_ObserverPattern;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
	private List<Observable> observables;
	
	public Sensor() {
		this.observables = new ArrayList<>();
	}
	
	public void informAll() {
		for (Observable o : observables) {
			o.inform();
		}
	}
	
	public void addObservable(Observable o) {
		this.observables.add(o);
	}
}
