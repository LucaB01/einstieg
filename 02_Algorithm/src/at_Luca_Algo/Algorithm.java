package at_Luca_Algo;

public interface Algorithm {
	public int[] doSort(int[] unsorted);
}
