package at_Luca_Algo;

public class Sorter {
	private Algorithm algo;
	
	public Sorter() {
		
	}
	
	public void setAlgo(Algorithm a) {
		this.algo = a;
	}
	
	public int[] doSort(int[] unsortedArray) {
		int[] sorted = algo.doSort(unsortedArray);
		return sorted;
	}
}
