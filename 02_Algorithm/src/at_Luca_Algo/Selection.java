package at_Luca_Algo;

public class Selection implements Algorithm{

	@Override
	public int[] doSort(int[] unsorted) {
		int n = unsorted.length; 
		  
        // One by one move boundary of unsorted subarray 
        for (int i = 0; i < n-1; i++) 
        { 
            // Find the minimum element in unsorted array 
            int min_idx = i; 
            for (int j = i+1; j < n; j++) 
                if (unsorted[j] < unsorted[min_idx]) 
                    min_idx = j; 
  
            // Swap the found minimum element with the first 
            // element 
            int temp = unsorted[min_idx]; 
            unsorted[min_idx] = unsorted[i]; 
            unsorted[i] = temp; 
        }
        return unsorted;
	}
}

