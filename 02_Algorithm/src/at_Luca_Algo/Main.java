package at_Luca_Algo;

import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		Sorter s1 = new Sorter();
		Bubble b = new Bubble();
		Insertion i = new Insertion();
		Selection s = new Selection();
		s1.setAlgo(s);
		int[] unsortedArray = new int[]{ 1,67,2,11,7,4,16 }; 
		System.out.println(Arrays.toString(s1.doSort(unsortedArray)));
	
	}
}
