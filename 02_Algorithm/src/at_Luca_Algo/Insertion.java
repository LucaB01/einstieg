package at_Luca_Algo;

public class Insertion implements Algorithm{

	@Override
	public int[] doSort(int[] unsorted) {
		int n = unsorted.length; 
        for (int i = 1; i < n; ++i) { 
            int key = unsorted[i]; 
            int j = i - 1; 
  
            /* Move elements of arr[0..i-1], that are 
               greater than key, to one position ahead 
               of their current position */
            while (j >= 0 && unsorted[j] > key) { 
            	unsorted[j + 1] = unsorted[j]; 
                j = j - 1; 
            } 
            unsorted[j + 1] = key; 
        }
        return unsorted;
	}

}
